package ui.jfx.components.filters;

public class NaturalsFilter extends Filter {
    public static String DEFAULT_FILTER = "[0-9]";
    public NaturalsFilter() {
        super(DEFAULT_FILTER);
    }
}
